document.addEventListener("deviceready", onDeviceReady, false);

var CONSUMER_SECRET = "McwJlMcvnnmJZLEbZgQjNYPeJSJhfIVe";
var CONSUMER_KEY = "MDODGpnRyzlNiMEuIkXQ";
//URLs
var BASEURL = "https://api.discogs.com";
var SEARCH = "database/search";
var ALBUM = "release_title";
var BARCODE = "barcode";
var RELEASE = "releases/";
var ARTIST = "artists/";
//Authentication String
var AUTH_STRING = "&key=" + CONSUMER_KEY + "&secret=" + CONSUMER_SECRET;
//Needed session data
var _previewListElements = null;
var _previewAlbum = null;
var _loadedAlbumCache = null;
var _fullAlbum = null;
var _albums = null;
var _artists = null;
var _myDB = null;
var _showDetailObj = null;

//this value is used for debugging purpose
var deleteDatabase = false;

//INIT
var myApp = new Framework7({
  modalTitle: 'MusicManagement',
  //pushState: true,
  domCache: true,
  onAjaxStart: function (xhr) {
    myApp.showIndicator();
  },
  onAjaxComplete: function (xhr) {
    myApp.hideIndicator();
  },
  onPageInit: function (app, page) {
    if (page.name === 'index') {
      enableFABonClick();
      if(_myDB != null){
        loadMusicData();
      }
    }
  }
});

var mainView = myApp.addView('.view-main', {
  dynamicNavbar: true
});

var $$ = Dom7;

var getJSON = function(url, callback) {
  var xhr = new XMLHttpRequest();
  xhr.open("get", url, true);
  xhr.responseType = "json";
  xhr.onload = function() {
    var status = xhr.status;
    if (status == 200) {
      callback(null, xhr.response);
    } else {
      callback(status);
    }
  };
  xhr.send();
};

//DEVICEREADY
function onDeviceReady() {
  _myDB = window.sqlitePlugin.openDatabase({name: "music.db", location: "default"});
  if (deleteDatabase) dropAllTables(_myDB);
  createTablesIfNotExisting(_myDB);
  loadMusicData();
}

//REGION --ONPAGEINIT--
myApp.onPageInit('preview', function (page) {
  if (_previewListElements != null) {
    loadPreviewPageWithData(_previewListElements);
  }
});

myApp.onPageInit('detail', function (page) {
  if (_previewAlbum != null) {
    $('#album_name').html(_previewAlbum.title);
    requestForAlbum(_previewAlbum);
    $$('#addToLibraryIcon').on('click', function (e) {
      if(_fullAlbum != null) {
        addNewAlbumToLibrary(_fullAlbum);
      }
    });
  }
});

myApp.onPageInit('detailpage', function (page) {
  if(_showDetailObj == null) return;
  $$('#deleteObject').on('click', function (e) {
    var deleteConfirm = confirm("Do you really want to delete the artist/album?")
    if(deleteConfirm) {
      //delete the object
      if(_showDetailObj != null) {
        var id = _showDetailObj.id;
        if(_showDetailObj.prototype == "Album") {
          deleteAlbum(_myDB, _showDetailObj);
        } else {
          deleteArtist(_myDB, _showDetailObj);
        }
        mainView.router.back();
        loadMusicData();
      }
    }
  });
  createUIForDetail(_showDetailObj);
});

//ENDREGION --ONPAGEINIT--

//REGION --ONCLICK FAB--
function enableFABonClick() {
  $$ = Dom7;
  $$('.plus').on('click', function (e) {
    var albumname = prompt("Please enter the name of the album", "Wolfmother");
    loadAlbumPreviewWithName(albumname);
  });

  $$('.icon-camera').on('click', function (e) {
    scan();
  });
}
//ENDREGION --ONCLICK FAB--

//REGION --SCAN
function scan() {
  cordova.plugins.barcodeScanner.scan(
    function (result) {
      if(!result.cancelled)
      {
        var conf = confirm("Is the code " + result.text + "correct?");
        if(!conf) return;
        loadAlbumPreviewWithBarcode(result.text);
      }
    },
    function (error) {
      console.log("Scanning failed: " + error);
      alert("Somethings not right, sorry! Try again!");
    }
  );
}

//ENDREGION --SCAN--

//REGION --PREVIEW--
function loadAlbumPreviewWithName(albumname) {
  if(albumname == null) return;
  var url = BASEURL + "/" + SEARCH + "?" + ALBUM + "=" + albumname + AUTH_STRING;
  loadPreview(url);
}

function loadAlbumPreviewWithBarcode(barcode) {
  if(barcode == null) return;
  var url = BASEURL + "/" + SEARCH + "?" + BARCODE + "=" + barcode + AUTH_STRING;
  loadPreview(url);
}

function loadPreview(url) {
  getJSON(url, function(err, data) {
    var results = data.results;
    if(results.length > 10) {
      results = results.slice(0,10);
    }
    _previewListElements = results;
    mainView.router.loadPage('preview.html');
  });
}

//creates the cards that are shown for the preview of the searched album
function loadPreviewPageWithData(data) {
  var index;
  var html = '<ul id="_previewListElements">';
  for (index = 0; index < data.length; ++index) {

    var img = data[index].thumb == "" ? "img/album-placeholder.png" : data[index].thumb;
    img = "'" + img + "'";
    var append = '<li class="card demo-card-header-pic previewElements" id="' + index + '">' +
    '<div style="background-image:url('+ img +')" valign="bottom" class="card-header color-white no-border"><h2>' + data[index].title + '</h2></div>' +
    '<div class="card-content">' +
    '<div class="card-content-inner">' +
    '<p class="color-gray">' + data[index].year + '</p>' +
    '<p>' + data[index].country + '</p>' +
    '</div>' +
    '</div> ' +
    '</li>';

    html = html + append;
  }
  html = html + "</ul>"
  $('#previewList').html(html);
  $$('.previewElements').on('click', function (e) {
    console.log(e.currentTarget.id + " was clicked");
    if(_previewListElements[e.currentTarget.id] != null) {
      createDetailViewForAlbum(_previewListElements[e.currentTarget.id]);
    }
  });
}

//ENDREGION --PREVIEW--

function createDetailViewForAlbum(element) {
  _previewAlbum = element;
  mainView.router.loadPage('detail.html');
}

function requestForAlbum(element) {
  if (_loadedAlbumCache != null && element == null) { element = _loadedAlbumCache; }
  var url = BASEURL + "/" + RELEASE + element.id + "?" + AUTH_STRING;
  getJSON(url, function(err, data) {
    var image = null;
    if (data.images != null) {
      image = new Image(data.images[0].height, data.images[0].width, data.images[0].resource_url, data.images[0].uri).toString()
    }
    var styles = "";
    if (data.styles != null) {
      data.styles.forEach(function(s) {
        styles = styles + s + " ";
      });
    }
    var album = new Album(data.id, data.artists[0].id, data.title, data.catno, data.year, data.resource_url,
      image, data.country, styles, data.thumb, null);

      var img = image == null ? "img/album-placeholder.png" : "'" + image.split(';')[3] + "'";
      var imgHTML = '<div style="background-image:url('+ img +')" valign="bottom" class="testheader"></div>'
      $('#albumImage').html(imgHTML);
      var songHTML = "";
      var songList = [];
      data.tracklist.forEach(function(entry) {
        var song = new Song(entry.title, data.id, entry.duration)
        var append = '<li class="item-content">' +
        '<div class="item-media"><i class="f7-icons size-22">right</i></div>' +
        '<div class="item-inner">' +
        '<div class="item-title">' + entry.title + '</div>' +
        '<div class="item-after">' + entry.duration + '</div>' +
        '</div>' +
        '</li> ';
        songHTML = songHTML + append;
        songList.push(song);
      });
      album.songList = songList;
      _fullAlbum = album;
      $('#textInfo1').html(data.country + ", " + data.year);
      $('#textInfo2').html(styles);
      $('#songList').html(songHTML);
    });
  }

  function stringToImage(imageString) {
    var arr = imageString.split(";");
    return new Image(arr[0], arr[1], arr[2], arr[3]);
  }

  function addNewAlbumToLibrary(album) {
    var url = BASEURL + "/" + ARTIST + album.artistID + "?" + AUTH_STRING;
    getJSON(url, function(err, data) {
      var image = null;
      if (data.images.length > 0) {
        image = new Image(data.images[0].height, data.images[0].width, data.images[0].resource_url, data.images[0].uri).toString()
      }
      var artist = new Artist(data.id, data.name, data.resource_url, image, data.release_url, data.profile, null);
      var i = 0;
      var members = [];
      if(data.members != null){
        for(i = 0; i < data.members.length; i++) {
          members.push(new Member(data.members[i].id, data.members[i].name, data.members[i].active, artist.id));
        }
      }
      artist.memberList = members;
      if(_myDB != null) {
        insertAlbumIntoDB(_myDB, album);
        insertArtistIntoDB(_myDB, artist);
      }
      goToStartAndResetGlobals();
    });
  }

  function loadMusicData() {
    getAllAlbums(_myDB, function(albums) {
      createUIForAlbums(albums);
    });
    getAllArtists(_myDB, function(artists){
      createUIForArtists(artists)
    });
  }

  function goToStartAndResetGlobals() {
    mainView.router.back({url: mainView.history[0], force: true});
    resetSessionData();
  }

  function resetSessionData() {
    _previewListElements = null;
    _previewAlbum = null;
    _loadedAlbumCache = null;
    _fullAlbum = null;
    _showDetailObj = null;
  }

  //REGION --UI generation--

  function createUIForDetail(obj) {
    var img = obj.image == null ? "img/album-placeholder.png" : "'" + obj.image.split(';')[3] + "'";
    var imgHTML = '<div style="background-image:url('+ img +')" valign="bottom" class="testheader"></div>'
    $('#detailImage').html(imgHTML);

    if (obj.prototype == "Album") {
      $('#detailName').html('<p> ' + obj.title + ' </p>');
      $('#textInfoDetail1').html('<p> ' + obj.year + ', ' + obj.country + ' </p>');
      $('#listHeader').html('<p> Songs: </p>');
      $('#textInfoDetail2').html('<p> ' + obj.styles + ' </p>');
      getSongsForAlbum(obj.id, _myDB, function(songs) {
        var songListHTML = generateList(true, songs);
        $('#detailList').html(songListHTML);
      });
    } else if(obj.prototype == "Artist") {
      $('#detailName').html('<p> ' + obj.name + ' </p>');
      $('#textInfoDetail1').html('<p> ' + obj.profile + ' </p>');
      $('#listHeader').html('<p> Members: </p>');
      getMemberForArtist(obj.id, _myDB, function(members){
        $('#textInfoDetail2').html('<p> Members: ' + members.length + ' </p>');
        var memberListHTML = generateList(false, members);
        $('#detailList').html(memberListHTML);
      });
    }
  }

  function generateList(isAlbum, objects) {
    var listHTML = "";
    if(isAlbum) {
      objects.forEach(function(entry) {
        var append = '<li class="item-content">' +
        '<div class="item-media"><i class="f7-icons size-22">play_round</i></div>' +
        '<div class="item-inner">' +
        '<div class="item-title">' + entry.title + '</div>' +
        '<div class="item-after">' + entry.duration + '</div>' +
        '</div>' +
        '</li> ';
        listHTML = listHTML + append;
      });
    } else {
      objects.forEach(function(entry) {
        var active = entry.active == 0 ? "not active" : "active";
        var append = '<li class="item-content">' +
        '<div class="item-media"><i class="f7-icons size-22">person</i></div>' +
        '<div class="item-inner">' +
        '<div class="item-title">' + entry.name + '</div>' +
        '<div class="item-after">' + active + '</div>' +
        '</div>' +
        '</li>';
        listHTML = listHTML + append;
      });
    }
    return listHTML;
  }

  function createUIForAlbums(albums) {
    _albums = albums;
    var html = "";
    var index;
    for (index = 0; index < albums.length; ++index) {
      var album = albums[index];
      var imgForAlbum = stringToImage(album.image);
      var img = "'" + imgForAlbum.uri + "'";
      var append = '<li class="card demo-card-header-pic albumElements" id="' + index + '">' +
      '<div style="background-image:url('+ img +')" valign="bottom" class="card-header color-white no-border"><h2>' + album.title + '</h2></div>' +
      '<div class="card-content">' +
      '<div class="card-content-inner">' +
      '<p class="color-gray">' + album.year + '</p>' +
      '<p>' + album.country + '</p>' +
      '</div>' +
      '</div> ' +
      '</li>';
      html = html + append;
    }
    $('#albumsCardList').html(html);
    $$('.albumElements').on('click', function (e) {
      console.log("Album " + e.currentTarget.id + " was clicked");
      _showDetailObj = _albums[e.currentTarget.id];
      _showDetailObj.prototype = "Album";
      mainView.router.loadPage('detailpage.html');
    });
  }

  function createUIForArtists(artists) {
    _artists = artists;
    var html = "";
    var index;
    for (index = 0; index < artists.length; ++index) {
      var artist = artists[index];
      var imgForArtist = stringToImage(artist.image);
      var img = "'" + imgForArtist.uri + "'";
      var card = '<li class="card demo-card-header-pic artistElements" id="' + index + '">' +
      '<div style="background-image:url('+ img +')" valign="bottom" class="card-header color-white no-border"><h2>' + artist.name + '</h2></div>' +
      '<div class="card-content">' +
      '<div class="card-content-inner">' +
      '</div>' +
      '</div> ' +
      '</li>';
      html = html + card;
    }
    $('#artistCardList').html(html);
    $$('.artistElements').on('click', function (e) {
      console.log("Artist " + e.currentTarget.id + " was clicked");
      _showDetailObj = _artists[e.currentTarget.id];
      _showDetailObj.prototype = "Artist";
      mainView.router.loadPage('detailpage.html');
    });
  }

  //ENDREGION --UI FOR STARTPAGE--
