//REGION --CREATE--

function createTablesIfNotExisting(db) {
  db.transaction(function(transaction) {
    //create table album
    transaction.executeSql('CREATE TABLE IF NOT EXISTS album (id integer primary key, artistID integer,' +
    'title text, catno text, year text, resource_url text, image text, country text, styles text, thumb text);', [],
    function(tx, result) {
      console.log("Table ALBUM created successfully if not already existing");
    },
    function(error) {
      console.log("Error occurred while creating the table.");
    });
    //create table artist
    transaction.executeSql('CREATE TABLE IF NOT EXISTS artist (id integer primary key, name text,' +
    'resourceURL text, image text, releaseURL text, profile text);', [],
    function(tx, result) {
      console.log("Table ARTIST created successfully if not already existing");
    },
    function(error) {
      console.log("Error occurred while creating the table.");
    });

    //create table member
    transaction.executeSql('CREATE TABLE IF NOT EXISTS member (id integer primary key, name text,' +
    'active integer, artistID integer);', [],
    function(tx, result) {
      console.log("Table MEMBER created successfully if not already existing");
    },
    function(error) {
      console.log("Error occurred while creating the table.");
    });

    //create table song
    transaction.executeSql('CREATE TABLE IF NOT EXISTS song (id integer primary key autoincrement, title text,' +
    'albumID integer, duration text);', [],
    function(tx, result) {
      console.log("Table SONG created successfully if not already existing");
    },
    function(error) {
      console.log("Error occurred while creating the table.");
    });

  });
}
//ENDREGION --CREATE--

//REGION --INSERT--

function insertAlbumIntoDB(db, album) {
  db.transaction(function(transaction) {
    var executeQuery = 'INSERT INTO album (id, artistID,' +
    'title, catno, year, resource_url, image, country, styles, thumb) VALUES (?,?,?,?,?,?,?,?,?,?)';
    console.log(executeQuery);
    transaction.executeSql(executeQuery, [album.id, album.artistID, album.title, album.catno, album.year, album.resource_url, album.image, album.country, album.styles, album.thumb]
      , function(tx, result) {
        console.log('Album ' + album.title + ' inserted');
        if(album.songList != null) {
          album.songList.forEach(function(song){
            insertSongIntoDB(db, song);
          });
        }
      },
      function(error){
        console.log('Error occurred' + error);
      });
    });
  }

  function insertArtistIntoDB(db, artist) {
    db.transaction(function(transaction) {
      var executeQuery = 'INSERT INTO artist (id, name, resourceURL,' +
      'image, releaseURL, profile) VALUES (?,?,?,?,?,?)';
      transaction.executeSql(executeQuery, [artist.id, artist.name, artist.resourceURL, artist.image, artist.releaseURL, artist.profile]
        , function(tx, result) {
          console.log('Artist ' + artist.name + ' inserted');
          if(artist.memberList != null) {
            artist.memberList.forEach(function(member){
              insertMemberIntoDB(db, member);
            });
          }
        },
        function(error){
          console.log('Error occurred');
        });
      });
    }

    function insertSongIntoDB(db, song) {
      db.transaction(function(transaction) {
        var executeQuery = 'INSERT INTO song (title, albumID,' +
        'duration) VALUES (?,?,?)';
        transaction.executeSql(executeQuery, [song.title, song.albumID, song.duration]
          , function(tx, result) {
            console.log('Song with title ' + song.title + ' inserted');
          },
          function(error){
            console.log('Error occurred');
          });
        });
      }

      function insertMemberIntoDB(db, member) {
        db.transaction(function(transaction) {
          var executeQuery = 'INSERT INTO member (id, name,' +
          'active, artistID) VALUES (?,?,?,?)';
          transaction.executeSql(executeQuery, [member.id, member.name, member.active ? 1 : 0, member.artistID]
            , function(tx, result) {
              console.log('Member with name ' + member.name + ' inserted');
            },
            function(error){
              console.log('Error occurred');
            });
          });
        }

        //ENDREGION --INSERT--

        //REGION --SELECT--

        function getAllAlbums(db, callback) {
          db.transaction(function(transaction) {
            transaction.executeSql('SELECT * FROM album', [], function (tx, results) {
              var len = results.rows.length, i;
              var returnArray = [];
              for (i = 0; i < len; i++){
                returnArray.push(results.rows.item(i));
              }
              callback(returnArray)
            }, null);
          });
        }

        function getAllArtists(db, callback) {
          db.transaction(function(transaction) {
            transaction.executeSql('SELECT * FROM artist', [], function (tx, results) {
              var len = results.rows.length, i;
              var returnArray = [];
              for (i = 0; i < len; i++){
                returnArray.push(results.rows.item(i));
              }
              callback(returnArray)
            }, null);
          });
        }

        function getSongsForAlbum(id, db, callback) {
          db.transaction(function(transaction) {
            transaction.executeSql('SELECT * FROM song WHERE albumID=?', [id], function (tx, results) {
              var len = results.rows.length, i;
              var returnArray = [];
              for (i = 0; i < len; i++){
                returnArray.push(results.rows.item(i));
              }
              callback(returnArray)
            }, null);
          });
        }

        function getMemberForArtist(id, db, callback) {
          db.transaction(function(transaction) {
            transaction.executeSql('SELECT * FROM member WHERE artistID=?', [id], function (tx, results) {
              var len = results.rows.length, i;
              var returnArray = [];
              for (i = 0; i < len; i++){
                returnArray.push(results.rows.item(i));
              }
              callback(returnArray)
            }, null);
          });
        }

        //ENDREGION --SELECT

        //REGION --DELETE--

        function deleteAlbum(db, album) {
          db.transaction(function(transaction) {
            var executeQuery = "DELETE FROM album where id=?";
            transaction.executeSql(executeQuery, [album.id],
              //On Success
              function(tx, result) {console.log('Delete album successfully');},
              //On Error
              function(error){console.log('Something went Wrong');});
            });
            db.transaction(function(transaction) {
              var executeQuery = "DELETE FROM song where albumID=?";
              transaction.executeSql(executeQuery, [album.id],
                //On Success
                function(tx, result) {console.log('Delete songs successfully');},
                //On Error
                function(error){console.log('Something went Wrong');});
              });
            }

            function deleteArtist(db, artist) {
              db.transaction(function(transaction) {
                var executeQuery = "DELETE FROM artist where id=?";
                transaction.executeSql(executeQuery, [artist.id],
                  //On Success
                  function(tx, result) {console.log('Delete artist successfully');},
                  //On Error
                  function(error){console.log('Something went Wrong');});
                });
                db.transaction(function(transaction) {
                  var executeQuery = "DELETE FROM member where artistID=?";
                  transaction.executeSql(executeQuery, [artist.id],
                    //On Success
                    function(tx, result) {console.log('Delete member successfully');},
                    //On Error
                    function(error){console.log('Something went Wrong');});
                  });
                }

                function dropAllTables(db) {
                  db.transaction(function(transaction) {
                    transaction.executeSql("DROP TABLE IF EXISTS album", [],
                    function(tx, result) {console.log('Table deleted successfully.');},
                    function(error){console.log('Error occurred while droping the table.');}
                  );
                  transaction.executeSql("DROP TABLE IF EXISTS artist", [],
                  function(tx, result) {console.log('Table deleted successfully.');},
                  function(error){console.log('Error occurred while droping the table.');}
                );
                transaction.executeSql("DROP TABLE IF EXISTS song", [],
                function(tx, result) {console.log('Table deleted successfully.');},
                function(error){console.log('Error occurred while droping the table.');}
              );
              transaction.executeSql("DROP TABLE IF EXISTS member", [],
              function(tx, result) {console.log('Table deleted successfully.');},
              function(error){console.log('Error occurred while droping the table.');}
            );
          });
        }

        //ENDREGION --DELETE--
