//the representation of an album
function Album(id, artistID, title, catno, year, resource_url, image, country, styles, thumb, songList) {
    this.id = id;
    this.title = title;
    this.artistID = artistID;
    this.catno = catno;
    this.year = year;
    this.resource_url = resource_url;
    this.image = image;
    this.country = country;
    this.styles = styles;
    this.thumb = thumb;
    this.songList = songList;
    //this.name = function() {return this.firstName + " " + this.lastName;};
}
//the representation of an artist
function Artist(id, name, resourceURL, image, releaseURL, profile, memberList) {
  this.id = id;
  this.name = name;
  this.resourceURL = resourceURL;
  this.image = image;
  this.releaseURL = releaseURL;
  this.profile = profile;
  this.memberList = memberList;
}
//the representation of an member
function Member(id, name, active, artistID) {
  this.id = id;
  this.name = name;
  this.active = active;
  this.artistID = artistID;
}
//the representation of a song
function Song(title, albumID, duration) {
  this.id = -1;
  this.title = title;
  this.albumID = albumID;
  this.duration = duration;
}
//the representation of an image
function Image(height, width, resource_url, uri) {
  this.height = height;
  this.width = width;
  this.resource_url = resource_url;
  this.uri = uri;
  this.toString = function() {return this.height + ";" + this.width + ";" + this.resource_url + ";" + this.uri}
}
